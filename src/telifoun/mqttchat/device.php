<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace telifoun\mqttchat;

class device{
    /**
     * 
     */
    const WEB="web";    
    /**
     *
     * @var type 
     */
    private $_id;
    private $_did;
    private $_name;
    private $_version;
    private $_voiceCAP=0;
    private $_videoCAP=0;
    private $_type;
    
    /**
     * 
     */
    public function __construct(){
      $browser = new \cbschuld\Browser();
      $this->_name=$browser->getBrowser();
      $this->_version=$browser->getVersion();
      $str=json_encode($browser->getBrowser(null,true).$browser->getVersion().$browser->getUserAgent().$browser->getPlatform());
      $this->_did= md5($str);
      $this->_type=device::WEB;     
    }    
    
        
    public function _getid(){
       return $this->_id;      
    }
    
        
    public function _getdid(){
       return $this->_did;      
    }
    
    public function _getname(){
      return $this->_name;      
    }
    
    public function _getversion(){
       return $this->_version;      
    }
    
    public function _isVoiceCAP(){
      return $this->_voiceCAP==1;      
    }
    
    public function _isVideoCAP(){
      return $this->_videoCAP==1;      
    }
    
    public function toArray(){
     return array("name"=>$this->_name,
                  "version"=>$this->_version,
                  "did"=>$this->_did,
                  "voiceCAP"=>$this->_voiceCAP,
                  "videoCAP"=>$this->_videoCAP,
                  "type"=>$this->_type)  ; 
    }
    
}
